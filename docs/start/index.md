---
title: "Markdown：语法简介"
slug: Start
date: 2022-05-05T11:32:59+08:00
draft: false
---




>Markdown是一种轻量级标记语言，它允许人们使用易读易写的纯文本格式编写文档，然后转换成有效的XHTML（或者HTML）文档。这种语言吸收了很多在电子邮件中已有的纯文本标记的特性。 ——[Markdown - 维基百科，自由的百科全书](https://zh.wikipedia.org/wiki/Markdown)

## 快速开始

您可以使用在线编辑器快速了解 Markdown 的编辑方式。Markdown 编辑方式与一般文本并无差异，只是引入了部分标记符号，以表明格式。

- [Online Markdown Editor - Dillinger, the Last Markdown Editor ever.](https://dillinger.io/)
- [Milkdown](https://milkdown.dev/online-demo)

## 下一步

您可以进一步了解这些标记符号的含义（ Markdown 语法），也可以先挑一款自己喜欢的编辑器。

- [挑选一款好用的编辑器](https://markdown.all2doc.com/editor/)
- [了解基础语法](https://markdown.all2doc.com/basic/)
<!-- 
## 为什么需要 markdown

现在常见的静态博客工具所使用的编辑方式都是markdown了。所以还是有必要学习一下的。 -->
<!-- 
> 例如，hugo生成的文章就是`\\content\\post\\***.md"`



```

## 关于图片的插入

Hugo是支持直接插入和引用本地图片的，有多种方式。我本人推荐使用Page Bundles，比较方便，并且其他markdown编辑器也能够识别。只需要新建页面时：

``` powershell
hugo new /post/new.md
#改成
hugo new /post/new/index.md
```

之后在`new`这个文件夹下添加图片图片，并直接引用文件名即可。VScode上有一键实现复制+引用的插件，可以参考[这篇文章](https://kermsite.com/p/vscode-markdown/)

目录结构：

``` txt
+---测试文章
|       cover.jpg
|       Design-V1.jpg
|       Design-V2.jpg
|       index.md
```

index.md文件内容：

``` markdown
---
title: "测试文章"
description: "文章简介"
date: "2020-08-10 01:00:00+0200"
slug: "test-post"
image: "cover.jpg"
categories:
    - 博客
tags:
    - Hugo
    - Stack
---

![图片 1](Design-V1.jpg)   

![图片 2](Design-V2.jpg)
```

也还有其他方法，不过我不太推荐，参考附录文章5-6。 -->

<!-- ## 附录

### 参考文献

1. [Markdown中插入视频 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/350887044)
2. [2020Typora小白完全使用教程 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/293557841)
3. [Markdown 语法快速入门手册_w3cschool](https://www.w3cschool.cn/markdownyfsm/markdownyfsm-odm6256r.html)
4. https://docs.stack.jimmycai.com
5. https://www.iterdaily.com/post/hugo_add_image_into_post/
6. https://blog.csdn.net/perfumekristy/article/details/122086009
7. https://www.rectcircle.cn/series/hugo/content-management/page-bundles/

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。 -->

## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
