---
title: "Editor"
slug: "Editor"
date: 2022-05-05T12:23:28+08:00
draft: false
---


## 编辑器的分类


市面上的 Markdown 编辑器非常多。基本可以分为所见即所得（ WYSIWYG ）和纯文本编辑两种。

这是由于 markdown 本身是带有标记的纯文本格式，通过软件渲染成漂亮的页面。部分软件提供即时的渲染，您无需接触源代码，就像使用 Word。而另一部分软件则认为对源代码编辑更加高效。

非所见即所得（Dillinger）：

![](2022-05-05-12-28-35.png)

所见即所得（Milkdown）：

![](2022-05-05-12-29-22.png)

另外，在非所见即所得的编辑器中，大部分编辑器均提供即时预览，即采取双栏方式，一侧为源代码编辑区，一侧为渲染结果区。我们在编辑时也能即时看到结果。

## 注意事项

选择编辑器时，请一定注意**插入功能和自动补全功能**。由于 Markdown 本身只是文本格式，故链接的插入和图片的插入都要手动配置。另外，自动补全也能够极大提高生产力。一款好的编辑器，应该支持：

- 从剪贴板直接插入链接，并生成链接标题（VSCode，Typora）
- 从剪贴板直接插入图片，保存到本地（VSCode，Typora），上传到图床（Typora），上传到Git（GitlabIDE）
- 自动补全列表（VSCode、Typora）
- 快速打开和保存（VSCode、Typora，GitlabIDE）

加分项：

- 强大的全文搜索功能（VSCode、Typora）
- Snippet、联想输入（VSCode）

## 网页编辑器

### Dillinger

支持从 GitHub 导入和导出。

[Online Markdown Editor - Dillinger, the Last Markdown Editor ever.](https://dillinger.io/)

### Milkdown

功能简单的在线编辑器。支持所见即所得。

[Milkdown](https://milkdown.dev/online-demo)

### GitlabIDE

不同于 GitHub，Gitlab 提供了一个强大的 IDE 界面。个人认为，如果您需要在线编辑 Markdown 文件，需要为静态博客添加后端的话，这是最佳选择。

您可以在 Gitlab 仓库 右上角选择 WebIDE 打开，也可以直接转到以下链接：

```
https://gitlab.com/-/ide/project/XXX
# 修改成你的仓库地址，如：
https://gitlab.com/-/ide/project/1kbtool/youhou/tree/main/-/
```

>注意：在多人协作时，使用界面上 WebIDE 打开，可能会出现404情况。

![](2022-05-05-13-35-37.png)

虽然 GitlabIDE 不支持所见即所得，也没有侧边预览，但是它有一个杀手锏功能。它支持直接从剪贴板导入图片、自动上传到仓库，并且生成引用。

![](2022-05-05-13-35-15.png)

## 桌面编辑器

### VSCode

**适合程序员**

开源。

需要仔细配置，配置好之后特别赞。尤其自带终端界面、自带 Git 管理、支持联想输入、支持全文搜索、与资源管理器联动、支持 Snippet 等功能，会让 markdown 写作更加轻松。

关于此部分具体配置，请参考：[用VScode写markdown - VSCode入门指南](https://vscode.all2doc.com/markdown/)

![](2022-05-05-13-47-30.png)

### Typora

**适合初学者**

所见即所得。界面好看。支持自动上传图片。写文章的体验不逊于任何一款笔记软件。最近改成收费了，Beta版还是一直免费。

Beta 下载地址：

- [Typora for windows — beta version release](https://typora.io/windows/dev_release.html)
- [Typora — macOS release channel](https://typora.io/releases/all)

当然也欢迎大家支持发行版，毕竟也不是特别的贵（非正当途径就算了。。。）：[Typora — a markdown editor, markdown reader.](https://typora.io/)


### Atom

开源。

[Atom](https://atom.io/)

[GitHub for Atom](https://github.atom.io/)

[用 Atom，在 Windows 上定制属于自己的 Markdown 编辑器 - 少数派 (sspai.com)](https://sspai.com/post/40460)

> 你也可以直接把图片拖拽进 Atom ，它会自动上传到图床，图床限定为 [imgur.com](http://imgur.com/) 和 [sm.ms](http://sm.ms/) 这两个。经过测试，[imgur.com](http://imgur.com/) 短时间内可以上传 25 张左右的图片（也可能是流量限制），墙内用户建议使用 [sm.ms](http://sm.ms/)，不限量免费上传图片，足够日常使用。但如果文章访问量较大，还是建议使用自己的 CDN。

[使用Atom打造无懈可击的Markdown编辑器 - 最骚的就是你 - 博客园 (cnblogs.com)](https://www.cnblogs.com/libin-1/p/6638165.html)

> 1. 使用截图工具将图片复制到系统剪切板。
> 2. 在Markdown新起一行输入文件名。
> 3. Ctrl + V 会自动把图片保存到Markdown文件相同目录下(因此要求Markdown文件应该先保存)，并命名为刚输入的文件名，同时在刚才输入文件名行处生成img标签。

### Marktext

- 开源。
- 所见即所得。
- 支持GitHub图床和SM图床。但是不支持Github套加速。

[marktext/zh_cn.md at develop · marktext/marktext (github.com)](https://github.com/marktext/marktext/blob/develop/docs/i18n/zh_cn.md#readme)


参考：[开源笔记软件MarkText安装，设置GitHub图床 - 哔哩哔哩 (bilibili.com)](https://www.bilibili.com/read/cv9721491)

![官图演示](https://github.com/marktext/marktext/raw/develop/docs/marktext.png?raw=true)


## 移动编辑器

移动端编辑器并不多，并且体验也一般。只列举一个安卓端的：

[Markor - Project Website &amp; FAQ - Markdown Editor, todo.txt, Android app](https://gsantner.net/project/markor.html)

## 附录

### Reference

1. [GitHub - mundimark/awesome-markdown-editors: A collection of awesome markdown editors &amp; (pre)viewers for Linux, Apple OS X, Microsoft Windows, the World Wide Web &amp; more](https://github.com/mundimark/awesome-markdown-editors)

### 版权声明

本文原载于https://all2doc.com
