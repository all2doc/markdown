---
title: "Markdown 进阶语法"
slug: "More"
date: 2022-05-05T13:54:30+08:00
draft: false
---

## 在行内代码中输入`

在行内代码中输入``需要采取以下形式

```
用两个反引号将一个反引号包裹。
``  `  ``
```

## 

由于`markdown`支持插入`html`语句，理论上可以插入任何内容。**但是不一定每一个软件都能够正确显示html内容。**

例如插入视频代码在`typora`里面能显示视频，传到`hugo`就直接一堆代码糊脸了。

> `hugo`也能插入视频，但是需要使用`shortcodes`，参考：[Hugo：短代码（shortcodes） (kermsite.com)](https://kermsite.com/p/hugo短代码shortcodes/)

### 插入视频

html中的[video标签](https://www.zhihu.com/search?q=video标签&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"article"%2C"sourceId"%3A350887044})

```html
<!-- mp4格式 -->
<video id="video" controls="" preload="none" poster="封面">
      <source id="mp4" src="mp4格式视频" type="video/mp4">
</videos>

<!-- webm格式 -->
<video id="video" controls="" preload="none" poster="封面">
      <source id="webm" src="webm格式视频" type="video/webm">
</videos>

<!-- ovg格式 -->
<video id="video" controls="" preload="none" poster="封面">
      <source id="ogv" src="ogv格式视频" type="video/ogv">
</videos>
```

### 插入（视频）小组件

html中的iframe标签

```html
<iframe 
src="视频或者网页路径" 
scrolling="no" 
border="0" 
frameborder="no" 
framespacing="0" 
allowfullscreen="true" 
height=600 
width=800> 
</iframe>
<!-- 相当于是子网页 -->
<!-- B站分享链接提供 -->

## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
